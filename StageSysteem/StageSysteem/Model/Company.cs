﻿using System;
namespace StageSysteem.Model
{
    public class Company : User
    {

        public string CompanyName { get; set; }
        public string Description { get; set; }
        public int Validated { get; set; }
        public int Public { get; set; }

        public Company()
        {
        }

        public void addInternship()
        {

        }

        public void editInternship(Internship id)
        {

        }

        public void deleteInternship()
        {

        }
    }
}
