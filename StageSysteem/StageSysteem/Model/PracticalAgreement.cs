﻿using System;
namespace StageSysteem.Model
{
    public class PracticalAgreement
    {
        public int Id { get; set; }
        public int ApplicationId { get; set; }
        public int Status { get; }
        public int Validated { get; set; }
        public int ValidatedBy { get; set; }
        public DateTime ValidatedDate { get; set; }

        public PracticalAgreement()
        {

        }

        public void Load(int id)
        {

        }

        public void getStatus(int id)
        {

        }
    }
}
