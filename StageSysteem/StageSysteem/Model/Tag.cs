﻿using System;
namespace StageSysteem.Model
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Crebo { get; }
        public int Visible { get; set; }

        public Tag()
        {
        }

        public void LoadTags()
        {

        }

        public void AddTag()
        {

        }

        public void EditTag()
        {

        }

        public void deleteTag()
        {

        }
    }
}
