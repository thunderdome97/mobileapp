﻿using System;
namespace StageSysteem.Model
{
    public class Internship
    {

        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int AvailableSpots { get; set; }
        public int Crebo { get; set; }
        public bool Visible { get; set; }

        public Internship()
        {
        }

        public void load()
        {

        }
    }
}
