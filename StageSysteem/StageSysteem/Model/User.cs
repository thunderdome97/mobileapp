﻿using System;
namespace StageSysteem.Model
{
    public abstract class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }

        public User()
        {
        }

        public bool Register()
        {

            return true;
        }

        public bool Login()
        {
            return true;
        }
    }
}
