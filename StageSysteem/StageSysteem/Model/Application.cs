﻿using System;
namespace StageSysteem.Model
{
    public class Application
    {

        public int Id { get; set; }
        public int InternshipId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Status { get; set; }
        public int Validated { get; set; }
        public string ValidatedBy { get; set; }
        public DateTime ValidatedDate { get; set; }

        public Application()
        {

        }

        public void loadApplication()
        {

        }
    }
}
