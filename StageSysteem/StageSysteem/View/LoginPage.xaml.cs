﻿using System;
using System.Collections.Generic;
using StageSysteem.View;

using Xamarin.Forms;

namespace StageSysteem.View
{
    public partial class LoginPage : ContentPage
    {

        public LoginPage()
        {
            InitializeComponent();
        }

        void Login_Clicked(System.Object sender, System.EventArgs e)
        {
            App.Current.MainPage = new MainPage();
        }
    }
}